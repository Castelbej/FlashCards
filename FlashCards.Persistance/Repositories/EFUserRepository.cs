﻿using FlashCards.Business;
using FlashCards.Persistance.EFContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlashCards.Persistance
{
    public class EFUserRepository : IUserRepository
    {
        private EntityDbContext _context;

        //Seed Db
        public EFUserRepository(EntityDbContext context)
        {
            _context = context;
            if (!_context.Users.Any())
            {
                _context.Users.AddRange(
                User.Create("tartampion@machin.ch", "12345678"),
                User.Create("rantanplan@siteweb.ch", "abcdefgh"),
                User.Create("gudru@internet.org", "1234abcd"),
                User.Create("default@FlashCards.ch", "default123")
                );
                _context.SaveChanges();
            }
        }
        public void AddUser(User user)
        {
            throw new NotImplementedException();
        }

        public User GetUserByEmail(string email)
        {
            return _context.Users.Where(x => x.Email == email).FirstOrDefault();
        }
    }
}
