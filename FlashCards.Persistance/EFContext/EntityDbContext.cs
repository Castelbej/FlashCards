﻿using FlashCards.Business;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Persistance.EFContext
{
    public class EntityDbContext : DbContext
    {
        public EntityDbContext() { }

        public EntityDbContext(DbContextOptions<EntityDbContext> options)
                                : base(options) { }


        public DbSet<User> Users { get; set; }
        public DbSet<Deck> Decks { get; set; }
        public DbSet<Card> Cards { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string connectionStr = "Server=(localdb)\\MSSQLLocalDB; Database=FlashCards; Trusted_Connection=True; MultipleActiveResultSets=true";
                optionsBuilder.UseSqlServer(connectionStr);
            }
        }
        
        //protected override void OnModelCreating(ModelBuilder modelbuilder)
        //{
        //    modelbuilder.Entity<DeckUser>().HasKey(du => new { du.DeckId, du.UserId });
        //}
    }
}
