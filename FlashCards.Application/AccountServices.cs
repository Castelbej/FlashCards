﻿using FlashCards.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Application
{
    public class AccountServices : IAccountServices
    {
        IUserRepository _userRepository;

        public AccountServices(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public UserDto Validate(string email, string password)
        {
            UserDto user = FindByEmail(email);
            if (user.Password == password)
                return user;

            return null;
        }

        public UserDto FindByEmail(string email)
        {
            User user = _userRepository.GetUserByEmail(email);

            if (user == null)
                return null;

            return AutoMapper.Mapper.Map<User, UserDto>(user);
        }
    }
}
