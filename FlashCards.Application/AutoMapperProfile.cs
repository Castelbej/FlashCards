﻿using AutoMapper;
using FlashCards.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Application
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
        }
    }
}
