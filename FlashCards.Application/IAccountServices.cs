﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Application
{
    public interface IAccountServices
    {
        UserDto Validate(string email, string password);
        UserDto FindByEmail(string email);
    }
}
