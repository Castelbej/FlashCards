﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using FlashCards.Application;
using AutoMapper;
using FlashCards.Persistance.EFContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using FlashCards.Business;
using FlashCards.Persistance;

namespace FlashCardsPresentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration) =>
            Configuration = configuration;
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperProfile>());

            services.AddDbContext<EntityDbContext>(options =>
                            options.UseSqlServer(Configuration["Data:FlashCards:ConnectionString"],
                            b => b.MigrationsAssembly("FlashCards.Presentation")));

            services.AddScoped<IAccountServices, AccountServices>();
            services.AddScoped<IUserRepository, EFUserRepository>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("Home/Error");
            }

            // Enable the usage of static content (HTML, CSS, JS, Images, ...)
            app.UseStaticFiles();

            // Setting the routing path 
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id ?}");
            });
        }
    }
}
