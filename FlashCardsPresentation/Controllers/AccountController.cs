﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlashCards.Application;
using FlashCardsPresentation.Models;
using Microsoft.AspNetCore.Mvc;

namespace FlashCardsPresentation.Controllers
{
    public class AccountController : Controller
    {
        IAccountServices _userServices;
        public AccountController(IAccountServices userServices)
        {
            _userServices = userServices;
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel loginViewModel, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userdto = _userServices.Validate(loginViewModel.Email, loginViewModel.Password);
                    if (userdto != null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError("LogInError", "e-mail ou mot de passe incorect");
                    return View();
                }
                catch (System.Exception)
                {
                    ModelState.AddModelError("LogInError", "e-mail ou mot de passe incorrect");
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
    }
}