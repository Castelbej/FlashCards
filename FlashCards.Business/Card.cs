﻿using FlashCards.Business.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Business
{
    public class Card : EntityBase<Guid>
    {
        public string Front { get; set; }
        public string Back { get; set; }

        public Card(string front, string back)
        {
            Front = front;
            Back = back;
        }
    }
}

