﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Business
{
    public interface IDeckRepository
    {
        void AddDeck();
        ICollection<Deck> GetDecksByUser();
    }
}
