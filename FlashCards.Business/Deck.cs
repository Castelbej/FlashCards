﻿using FlashCards.Business.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Business
{
    public class Deck : EntityBase<Guid>
    {
        public virtual ICollection<Card> Cards { get; set; }
        public string Title { get; set; }
        //may be used in the future to make the deck visible for other users (true)
        public bool Scope { get; set; }
        public string Language { get; set; }

        public static Deck Create(string title, string language, bool scope, ICollection<Card> cards = null)
        {
            Deck deck = new Deck
            {
                Title = title,
                Language = language,
                Scope = scope,
                Cards = cards,
            };
            return deck;
        }
    }
}
