﻿using FlashCards.Business.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Business
{
    public class User : EntityBase<Guid>
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public static User Create(string email, string password)
        {
            User user = new User
            {
                Email = email,
                Password = password
            };

            return user;
        }
    }
}
