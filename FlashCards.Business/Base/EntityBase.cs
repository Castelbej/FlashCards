﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Business.Base
{
    public abstract class EntityBase<TId>
    {
        public TId Id { get; protected set; }
    }
}
