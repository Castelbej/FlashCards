﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlashCards.Business
{
    public interface IUserRepository
    {
        User GetUserByEmail(string email);
        void AddUser(User user);
    }
}
